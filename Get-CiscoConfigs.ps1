<# .SYNOPSIS
	Get-CiscoConfigs.ps1
.DESCRIPTION
	This script copies files Cisco device's configurations to a file.
	The CredMan.ps1 script must be in the same directory as this script.
	The account used to login to the devices must be added to Windows Credential Manager.
    Change the first part of the $Results variable where it shows "Switch" to the URL you
    set your credentials to in Creadential Manager.
	This script uses plink.exe. It can be download from the same place as putty.exe
.NOTES
	Create by: rabel001@gitlab.com
	Creation Date: 2/27/2018
.EXAMPLE
	Get-CiscoConfigs.ps1 -IPAddress 10.10.10.10 -ConfigName SWITCH1
	This example will copy the device configs to the $Global:BackupPath. You can
	change the variable to whatever path you're like. This eliminates the need for 
	adding the -ConfigDestination switch. The default command is "show start"
.EXAMPLE 
	Get-CiscoConfigs.ps1 -IPAddress 10.10.10.10 -Command "show run"
	This example allows you to be able to run a different command from the default
	"show start" command.
.EXAMPLE
	Get-CiscoConfigs.ps1 -IPAddress 10.10.10.10 -ConfigDestination C:\Configs
	This example lets you specify a different destination for the config file than
	the default directory set by the variable $Global:BackupPath.
.EXAMPLE
	Get-CiscoConfigs.ps1 -IPAddress 10.10.10.10 -Command "show run" -ConfigDestination C:\Configs
	This example shows the combination of all switches available for this script.
#>

Param (
	[parameter(Mandatory = $True, Position = 0)][string]$IPAddress,
	[parameter(Mandatory = $True, Position = 1)][string]$ConfigName,
	[parameter(Mandatory = $False, Position = 2)]$Command,
	[parameter(Mandatory = $False, Position = 3)][string]$ConfigDestination
)

$Global:BackupPath = "$($env:USERPROFILE)\Documents\Cisco_Configs";

TRY
{
	. .\CredMan.ps1;
}
CATCH
{
	Write-Host "`n ERROR: CreadMan.ps1 is missing.`nExiting!`n" -ForegroundColor Red;
	Exit;
}

#Gets creadentials from Windows Credential Manager and set $Cred to hold the information.
[void]([PsUtils.CredMan+Credential] $Cred = New-Object PsUtils.CredMan+Credential);
[PsUtils.CredMan]::CredRead("Switches", [PsUtils.CredMan+CRED_TYPE]::GENERIC, [ref]$Cred);

$Plink = "c:\Program Files (x86)\Putty\plink.exe";
if (!(Test-Path $Plink))
{
	Write-Host " Please specify the location of plink.exe." -ForegroundColor Yellow;
	Write-Host " Location:" -NoNewLine -ForegroundColor Yellow;
	$Plink = Read-Host
	$firstChar = $Plink.Substring(0, $Plink.Length - $Plink.Length + 1);
	$lastChar = $Plink.Substring($Plink.Length - 1);
	if ($firstChar -eq '"' -and $lastChar -eq '"') {$Plink = $Plink.Substring(1, $Plink.Length - 2)}
	if (!($Plink.Substring($Plink.Length - 9) -match "plink.exe"))
	{
		if ($Plink.Substring($Plink.Length - 1) -eq '\') { $Plink = "$($Plink)plink.exe"}
		elseif ($Plink.Substring($Plink.Length - 1) -ne '\') { $Plink = "$($Plink)\plink.exe"}
	}
	Write-Host "`n Using `"$($Plink)`"." -ForegroundColor Yellow
	if (!(Test-Path $Plink))
 	{
		Write-Host "`n Plink.exe not found.`nExiting!" -ForegroundColor Red;
		Exit;
	}
}

function CreateConfigDir
{
	if (![String]::IsNullOrEmpty($ConfigDestination) -and !(Test-Path $ConfigDestination))
	{
		$Global:BackupPath = $ConfigDestination;
		Write-Host "`n Create new backup directory `"" -NoNewLine -ForegroundColor Yellow;
		Write-Host "$($Global:BackupPath)" -NoNewLine -ForegroundColor White;
		Write-Host "`"?" -ForegroundColor Yellow;
		Write-Host " Yes [Y], No [N] (default): " -NoNewLine -ForegroundColor Yellow;
		$response = Read-Host;
		if ($response.ToLower() -eq "y" -or $response.ToLower() -eq "yes")
		{ 
			if (!(Test-Path $Global:BackupPath))
			{
				New-Item -Path $Global:BackupPath -ItemType Directory -ErrorAction SilentlyContinue -ErrorVariable direrr | Out-Null
				if ($direrr)
				{
					Write-Host " There was an error creating new directory: $($Global:BackupPath)." -ForegroundColor Red
					Write-Host " Using `"$($env:USERPROFILE)\Documents`" instead." -ForegroundColor Red
					$Global:BackupPath = "$($env:USERPROFILE)\Documents"
				}
				else
				{
					Write-Host "`n New directory `"$($Global:BackupPath)`" has been created." -ForegroundColor Yellow
				}
			}
			else
			{
				Write-Host " $($Global:BackupPath) already exists. Use this path?" -ForegroundColor Yellow
				Write-Host " Yes [Y], No [N] (default): " -NoNewLine -ForegroundColor Yellow
				$BackupPathResponse = Read-Host
				if ($BackupPathResponse -eq "N" -or $BackupPathResponse -eq "No" -or $rBackupPathResponse -eq "n" -or $BackupPathResponse -eq "no")
				{
					$Global:BackupPath = "$($env:USERPROFILE)\Documents"
					Write-Host "`n Using $($Global:BackupPath)." -ForegroundColor Yellow
				}
				elseif ($BackupPathResponse -eq "Y" -or $BackupPathResponse -eq "Yes" -or $rBackupPathResponse -eq "y" -or $BackupPathResponse -eq "y")
				{
					$Global:BackupPath = "$($env:USERPROFILE)\Documents\configs"
				}
				else
				{
					Write-Host " `"$($BackupPathResponse)`" is not a recognized command." -ForegroundColor Red
					CreateConfigDir
				}
			}
		}
		elseif ($response.ToLower() -eq "n" -or $response.ToLower() -eq "no")
		{
			$Global:BackupPath = "$($env:USERPROFILE)\Documents"
			Write-Host "Using `"$($env:USERPROFILE)\Documents`"." -ForegroundColor Yellow
		}
		else
		{
			Write-Host " `"$($response)`" is not a recognized command." -ForegroundColor Red
			CreateConfigDir
		}
	}
	elseif (![String]::IsNullOrEmpty($ConfigDestination) -and (Test-Path $ConfigDestination))
	{
		$Global:BackupPath = $ConfigDestination
		Write-Host "`n Using `"$($ConfigDestination)`"." -ForegroundColor Yellow
	}
}
CreateConfigDir

if ([String]::IsNullOrEmpty($Command))
{
	$Command = "show run"
}

function showCopyRenameMenu
{
	#Clear-Host;
	Write-Host "`n " -NoNewLine;
	Write-Host "WARNING:" -NoNewLine -ForegroundColor Black -BackgroundColor Yellow;
	Write-Host " $($Global:BackupPath)\$($ConfigName).txt" -NoNewline -ForegroundColor Cyan;
	Write-Host " already exists." -ForegroundColor Yellow;
	Write-Host " Would you like to copy over it or rename it?" -ForegroundColor Yellow;
	Write-Host " Copy over [C], Rename [R] (default): " -NoNewLine -ForegroundColor Yellow;
	$responseForFile = Read-Host;
	return $responseForFile.ToLower();	
}

$global:notReadToCopy = $true;
function rename
{
	$newFile = "$($ConfigName)_$((get-date).ToString("MM-dd-yyyy_hhmmss")).bak"
	Write-Host "`n Rename `"$($ConfigName).txt`" to `"$($newFile)`"" -ForegroundColor Cyan
	Move-Item -Path "$($Global:BackupPath)\$($ConfigName).txt" -Destination "$($Global:BackupPath)\$($newFile)"
	$global:notReadToCopy = $false;
}

function copyOverOrRename($strResponse)
{
	if([string]::IsNullOrEmpty($strResponse))
	{
		rename;
	}
	else
	{
		switch($strResponse)
		{
			"r" { 
				rename;
				break;
			}
			"rename" {
				rename;
				break;
			}
			
			"c" { 
				$global:notReadToCopy = $false;
				break;
			}
			"copy" {
				$global:notReadToCopy = $false;
				break;
			}
			default {
				Write-Host "`n Selection out of range." -ForegroundColor Red;
				Write-Host "`n Press [ENTER] to try again: " -NoNewline -ForegroundColor Yellow
				Read-Host;
				copyOverOrRename(showCopyRenameMenu);
				break;
			}
		}
	}
}
do {
	if (Test-Path "$($Global:BackupPath)\$($ConfigName).txt")
	{
		copyOverOrRename(showCopyRenameMenu);
	}
	else 
	{
		$global:notReadToCopy = $false;	
	}
} while($global:notReadToCopy)


Write-Host "`n Creating config file $($Global:BackupPath)\$($ConfigName).txt`n" -ForegroundColor Cyan
& $Plink -ssh $IPAddress -l $Cred.UserName -pw $Cred.CredentialBlob -batch $Command | Out-File -FilePath "$($Global:BackupPath)\$($ConfigName).txt"
#& $Plink -ssh $IPAddress -l ($Credentials.GetNetworkCredential().Username) -pw ($Credentials.GetNetworkCredential().Password) -batch $Command | Out-File -FilePath "$($BackupPath)\$($ConfigName).txt"