<# .SYNOPSIS
	Get-CiscoMenu.ps1
.DESCRIPTION
    This script is menu based with the options to back up configs on all switches
    designated in teh sitches.xml file. If also gives you the option to select a
    single switch and the ability to specify a different command other than the
    default "show start" command.

    The switches.xml file must be in the same location as this script.

    This script also requires Get-CiscoConfigs.ps1 to be in the same location.
.NOTES
	Create by: rabel001@gitlab.com
	Creation Date: 2/27/2018
#>

Clear-Host;

$DoAllSwitches = $false;
$Global:doOutput = $false;
$Global:destination = "$($env:USERPROFILE)\Documents\Cisco_Configs";
function Get-ScriptDirectory
{
	$Invocation = (Get-Variable MyInvocation -Scope 1).Value
	Split-Path $Invocation.MyCommand.Path
}
$scriptPath = Get-ScriptDirectory

#This can be changed to whatever location you want.
$xmlSwitches = [xml](gc "$($scriptPath)\Switches.xml");
$switches = $xmlSwitches.Switches.Switch;

Write-Host "`n Would you like to specify a destination for the config files?" -ForegroundColor Yellow;
Write-Host " Default location: " -NoNewLine -ForegroundColor Yellow;
Write-Host "$($Global:destination)" -ForegroundColor Cyan;
Write-Host " Yes [Y], No [N] (default): " -NoNewline -ForegroundColor Yellow;
$response = Read-Host;

function directoryQ
{
	Write-Host "`n Specify the directory for saving config files: " -NoNewline -ForegroundColor Yellow;
	$Global:destination = Read-Host;
}

if ($response.ToLower() -eq "y")
{
	directoryQ;
	if (!(Test-Path $Global:destination))
 	{
		Write-Host "`n The directory " -NoNewLine -ForegroundColor Yellow;
		Write-Host "`"$($Global:destination)`"" -NoNewline
		Write-Host " does not exist. Press [ENTER] to change the destination." -ForegroundColor Yellow;
		directoryQ
	}
}

Clear-Host;
Write-Host "`n##################################################`n" -ForegroundColor Yellow
$i = 1;
foreach ($switch in $switches)
{
	Write-Host " $($switch.Order)) $($switch.Name) - $($switch.IP)" -ForegroundColor Yellow;
	$i++
}
$i++;
Write-Host " $($i)) Select all" -ForegroundColor Yellow;
Write-Host " $($i+1)) Quit" -ForegroundColor Yellow;

Write-Host "`n##################################################" -ForegroundColor Yellow;
Write-Host "`n Which switch would you like to work with? " -NoNewline -ForegroundColor Yellow;
$response = Read-Host;

[int]$intTest = $null;
if (!([int32]::TryParse($response , [ref]$intTest)))
{
	Write-Host "`nInput string was not in a correct format." -ForegroundColor Red;
	$response = Read-Host;
	. .\Cisco-Menu.ps1;
}

if (!([String]::IsNullOrEmpty($response)) -and ([int]$response -le $i))
{
	if ([int]$response -eq $i)
 {
		$DoAllSwitches = $true;
		Write-Host "`n All switches have been selected.`n" -ForegroundColor Yellow;
		$allCommands = @();
		foreach ($switch in $switches)
		{
			$value = $switch.Order;
			$ipAddress = $switch.IP;
			$switchName = $switch.Name;
			$configName = $switchName;
			$allCommands += "$($switchName),.\Get-CiscoConfigs.ps1 -IPAddress $($ipAddress)";
		}
	}
	elseif ($response.Length -eq 1)
 	{
		$ipAddress = $switches.Where( {$_.Order -eq "0$($response)"}).IP;
		$switchName = $switches.Where( {$_.Order -eq "0$($response)"}).Name;
		$configName = $switchName;
	}
	elseif ($response.Length -eq 2)
 	{
		$ipAddress = $switches.Where( {$_.Order -eq "$($response)"}).IP;
		$switchName = $switches.Where( {$_.Order -eq "$($response)"}).Name;
		$configName = $switchName;
	}
	#Write-Host "`n$($ipAddress)" -ForegroundColor Cyan
	#Write-Host "`n$($switchName)" -ForegroundColor Cyan
	Clear-Host;
}
elseif ($response -gt $i)
{
	if ($response -eq $i + 1)
 {
		Clear-Host;
		Write-Host "`n##################################################`n" -ForegroundColor Yellow;
		Write-Host "`n Quiting`n" -ForegroundColor Yellow;
		Write-Host "`n##################################################`n" -ForegroundColor Yellow;
		Exit;
	}
	Write-Host "`n Selection out of range." -ForegroundColor Red;
}
else
{
	Write-Host "`nYou did not enter anything." -ForegroundColor Red;
}

if ($response -ne $i) { $topTitle = "$($switchName) - IP Address $($ipAddress)`n"; }
else { $topTitle = "All Switches`n" }
if ($response -ne $i) { $topTitle = "$($switchName) - IP Address $($ipAddress)`n"; }
else { $topTitle = "All Switches`n" }
Write-Host "`n###########################################################`n" -ForegroundColor Yellow;
Write-Host " Switch: $($topTitle)" -ForegroundColor Yellow;
Write-Host " 01) Backup startup-configuration." -ForegroundColor Yellow;
$item01 = "show start";
Write-Host " 02) Enter command." -ForegroundColor Yellow;
Write-Host "`n###########################################################`n" -ForegroundColor Yellow;
Write-Host " Select item: " -NoNewline -ForegroundColor Yellow;
$response = Read-Host;

if ($response.Length -eq 1)
{
	$item = "0$($response)";
}
else
{
	$item = $response;
}

Clear-Host;
$manualCommand = $null;
switch ($item)
{
	"01" {
  		Write-Host "`n Running command $($item01) against $($switchName)." -ForegroundColor Cyan;
		$runItem = $item01;
	}
	"02" {
		Write-Host "`n###########################################################`n" -ForegroundColor Yellow;
		Write-Host " Enter the command: " -NoNewline -ForegroundColor Yellow;
		$manualCommand = Read-Host;
		Write-Host "`n###########################################################`n" -ForegroundColor Yellow;
		#Write-Host " You entered: " -NoNewline -ForegroundColor Yellow;
		#Write-Host "$($manualCommand)" -ForegroundColor Cyan;
		$runItem = $manualCommand;
		if (!$DoAllSwitches)
		{
			function displayOrSave
			{
				Write-Host "`n Would you like to display the output or copy it to a config file? " -ForegroundColor Yellow;
				Write-Host " Display output [D], Save config [S] (default): "  -NoNewline -ForegroundColor Yellow;
				$response = Read-Host;
				return $response;
			}
			$response2 = displayOrSave;
			#Write-Host "HERE'S THE RESPONSE $($response)"
			#Read-Host
			if ($response2.ToLower() -eq "d")
			{
				$Global:doOutput = $true;
			}
			elseif ($response2.ToLower() -eq "s" -or [string]::IsNullOrEmpty($response.ToLower()))
			{
				Write-Host "`n Enter new config name: " -NoNewline -ForegroundColor Yellow;
				$configName = Read-Host;
			}
			else
			{
				Write-Host "`n Incorrect response." -ForegroundColor Red;
				displayOrSave;
			}
		}
		Write-Host "`n Running command $($runItem) against $($switchName)." -ForegroundColor Cyan;
	}
	default { Write-Host "`n You didn't enter a corrent item." -ForegroundColor Red; Exit; }
}

if ($DoAllSwitches)
{
	Clear-Host
	foreach ($command in $allCommands)
 {
		$switchName = $command.Split(',')[0];
		$nCommand = $command.Split(',')[1];
		Write-Host "`n###########################################################" -ForegroundColor Yellow;
		if ($manualCommand)
		{
			#function newConfigName
			#{
			#Param(
			#	[Parameter(Mandatory = $true, HelpMessage="Test")][ValidateScript({$switchName})]$Name
			#)
			#$name
			#}
			#newConfigName
			#Add-Type -AssemblyName Microsoft.VisualBasic
			#$newConfigName = [Microsoft.VisualBasic.Interaction]::InputBox("Enter config file name for $($switchName): ","Config File Name", "$($switchName)");
			#Write-Host "`n Would like to enter a file name or use the switch name for the config file?" -ForegroundColor Yellow;
			#Write-Host " Enter new name (N), Use switch name (S): " -NoNewline -ForegroundColor Yellow
			#$response = Read-Host;
			#if($response.ToLower() -eq "n")
			#{
			#	$newConfigName = $switchName;
			#}
			#else
			#{
			Write-Host "`n Enter config file name for $($switchName): " -NoNewline -ForegroundColor Yellow;
			$newConfigName = Read-Host;
			$configName = $newConfigName;
			#}
		}
		Write-Host "`n Running command `"$($runItem)`" against $($switchName)." -ForegroundColor Cyan;
		#Write-Host "`n Downloading configs from $($switchName)." -ForegroundColor Yellow;
		Write-Host "`n $($nCommand) -ConfigName $($switchName) -Command `"$($runItem)`"" -ForegroundColor Yellow;
		Invoke-Expression "$($nCommand) -ConfigName $($switchName) -Command `"$($runItem)`"";
		Start-Sleep -Seconds 5;
	}
}
else
{
	if ($Global:doOutput)
 {
		.\Run-CiscoCommand.ps1 -IPAddress $ipAddress -Command "`"$($runItem)`"";
	}
	else
 {
		#Write-Host "`n Running command $($runItem) against $switchName." -ForegroundColor Cyan;
		.\Get-CiscoConfigs.ps1 -IPAddress $ipAddress -ConfigName $configName -Command "`"$($runItem)`"" -ConfigDestination $Global:destination;
	}
}

Write-Host "`n Command comeplete.`n`n Would you like to run another command or quit?" -ForegroundColor Yellow;
Write-Host " Continue [C] (default), Quit [Q]: " -NoNewline -ForegroundColor Yellow;
$response = Read-Host;
if([string]::IsNullOrEmpty($response))
{
	. .\Cisco-Menu.ps1;
}
else
{
	switch($response) {
		"c" { . .\Cisco-Menu.ps1; }
		"C" { . .\Cisco-Menu.ps1; }
		"Continue" { . .\Cisco-Menu.ps1; }
		"continue" { . .\Cisco-Menu.ps1; }
		defualt { Exit; }
	}
}

